module.exports = function(sequelize, DataTypes) {
    var Wine = sequelize.define('Wine', {
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
        countryId: {
            type: DataTypes.INTEGER,
            validate: {
                min: 1,
                notNull: true
            }
        },
        grapeId: {
            type: DataTypes.INTEGER,
            validate: {
                min: 1,
                notNull: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                Wine.belongsTo(models.Country);
                Wine.belongsTo(models.Grape);
            }
        }
    });

    return Wine;
};
