module.exports = function(sequelize, DataTypes) {
    var Country = sequelize.define('Country', {
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                Country.hasMany(models.Wine)
            }
        }
    });

    return Country;
};
