module.exports = function(sequelize, DataTypes) {
    var Grape = sequelize.define('Grape', {
        name: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                Grape.hasMany(models.Wine)
            }
        }
    });

    return Grape;
}
