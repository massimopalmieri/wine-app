/**
 * Module dependencies.
 */

var express      = require('express'),
    router       = require('./router'),
    http         = require('http'),
    path         = require('path'),
    db           = require('./models'),
    chromelogger = require('chromelogger');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// Favicon
app.use(express.favicon());

// Parse body
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.logger('dev'));

// Router
app.use(app.router);

// Cross origin headers
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});


// Serve static assets
app.use(express.static(path.join(__dirname, 'public')));

// Logger
app.use(chromelogger.middleware);

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

router(app);


http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
