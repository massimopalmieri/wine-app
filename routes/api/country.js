var db = require('../../models');


/*
 * GET countries.
 */

exports.getCountry = function(req, res) {

    if (req.params.id) {
        db.Country.find({
            where: {
                id: req.params.id
            }
        }).success(function(result) {
            res.send(result);
        });
    } else {
        db.Country.findAll().success(function(result) {
            res.send(result);
        });
    }
};


/*
 * POST country.
 */

exports.postCountry = function(req, res) {
    db.Country.create({
        name: req.body.name,
        createdAt: new Date(),
        updatedAt: new Date()
    }).success(function(result) {
        res.send(result);
    }).error(function(error) {
        res.send(error);
    });
};


/*
 * PUT country.
 */

exports.putCountry = function(req, res) {
    var country = db.Country.find({
        where: {
            id: req.params.id
        }
    });

    country.success(function(country) {
        country.updateAttributes({
            name: req.body.name,
            updatedAt: new Date()
        }).success(function(result) {
            res.send(result);
        }).error(function(error) {
            res.send(error);
        });
    });
};


/*
 * DELETE country.
 */

exports.deleteCountry = function(req, res) {
    var country = db.Country.find({
        where: {
            id: req.params.id
        }
    });

    country.success(function(country) {
        country.destroy().success(function() {
            res.send();
        }).error(function(error) {
            res.send(error);
        });
    });
};
