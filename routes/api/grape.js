var db = require('../../models');

/*
 * GET grapes.
 */

exports.getGrape = function(req, res) {

    if (req.params.id) {
        db.Grape.find({
            where: {
                id: req.params.id
            }
        }).success(function(result) {
            res.send(result);
        });
    } else {
        db.Grape.findAll().success(function(result) {
            res.send(result);
        });
    }
};


/*
 * POST grape.
 */

exports.postGrape = function(req, res) {
    db.Grape.create({
        name: req.body.name,
        createdAt: new Date(),
        updatedAt: new Date()
    }).success(function(result) {
        res.send(result);
    }).error(function(error) {
        res.send(error);
    });
};


/*
 * PUT grape.
 */

exports.putGrape = function(req, res) {
    var grape = db.Grape.find({
        where: {
            id: req.params.id
        }
    });

    grape.success(function(grape) {
        grape.updateAttributes({
            name: req.body.name,
            updatedAt: new Date()
        }).success(function(result) {
            res.send(result);
        }).error(function(error) {
            res.send(error);
        });
    });
};


/*
 * DELETE grape.
 */

exports.deleteGrape = function(req, res) {
    var grape = db.Grape.find({
        where: {
            id: req.params.id
        }
    });

    grape.success(function(grape) {
        grape.destroy().success(function() {
            res.send();
        }).error(function(error) {
            res.send(error);
        });
    });
};
