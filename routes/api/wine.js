var db = require('../../models');


/*
 * GET wines.
 */

exports.getWine = function(req, res) {
    if (req.params.id) {
        db.Wine.find({
            include: [db.Country, db.Grape],
            where: {
                id: req.params.id
            }
        }).success(function(wine) {
            res.send(wine);
        });
    } else {
        var limit = req.query.limit || 2,
            page = req.query.page || 1;

        db.Wine.findAndCountAll({
            include: [db.Country, db.Grape],
            offset: req.query.page * limit - limit,
            limit: limit
        }).success(function(result) {
            res.send(result.rows);
        });
    }
};


/*
 * POST wine.
 */

exports.postWine = function(req, res) {
    db.Wine.create({
        name: req.body.name,
        countryId: req.body.countryId,
        grapeId: req.body.grapeId,
        createdAt: new Date(),
        updatedAt: new Date()
    }).success(function(wine) {
        res.send(wine);
    }).error(function(error) {
        res.send(error);
    });
};


/*
 * PUT wine.
 */

exports.putWine = function(req, res) {
    var wine = db.Wine.find({
        where: {
            id: req.params.id
        }
    });

    wine.success(function(wine) {
        wine.updateAttributes({
            name: req.body.name,
            countryId: req.body.countryId,
            grapeId: req.body.grapeId,
            updatedAt: new Date()
        }).success(function(wine) {
            res.send(wine);
        }).error(function(error) {
            res.send(error);
        });
    });
};


/*
 * DELETE wine.
 */

exports.deleteWine = function(req, res) {
    var wine = db.Wine.find({
        where: {
            id: req.params.id
        }
    });

    wine.success(function(wine) {
        wine.destroy().success(function() {
            res.send();
        }).error(function(error) {
            res.send(error);
        });
    });
};
