var Promise = require('bluebird'),
    db = require('../models');

/*
 * GET home page.
 */

exports.index = function(req, res) {
    // var grapesResult, countriesResult;

    // db.Country.findAll().success(function(countries) {
    //     db.Grape.findAll().success(function(grapes) {
    //         res.render('index', {
    //             title: 'Express',
    //             countries: countries,
    //             grapes: grapes
    //         });
    //     });
    // });

    var countries = db.Country.findAll(),
        grapes = db.Grape.findAll();

    Promise.all([countries, grapes]).then(success, failure);

    function success(response) {
        res.render('index', {
            title: 'Express',
            countries: response[0],
            grapes: response[1]
        });
        // not sure what response is, perhaps an array with both responses?
    }

    function failure() {
        console.log('failure');
    }
};


/*
 * GET wines page.
 */

exports.wines = function(req, res) {
    db.Wine.findAll({
        include: [db.Country, db.Grape]
    }).success(function(wines) {
        res.render('wines', {
            title: 'Express',
            wines: wines
        });
        res.send(wines);
    });
};


/*
 * POST wines page.
 */

exports.newWines = function(req, res) {
    db.Wine.create({
        name: req.body.name,
        countryId: req.body.countryId,
        grapeId: req.body.grapeId,
        createdAt: new Date(),
        updatedAt: new Date()
    }).success(function(wine) {
        res.redirect('/');
    }).error(function(error) {
        res.send(error);
    });
};
