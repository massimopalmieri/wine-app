var routes = require('./routes'),
    user   = require('./routes/user'),
    api    = require('./routes/api');


module.exports = function(app) {
    app.get('/', routes.index);

    // get all the wines
    app.get('/wines', routes.wines);

    // create a new wine
    app.post('/wines', routes.newWines);

    app.get('/users', user.list);


    // API

    /* ---- WINE ---- */

    // get wine
    app.get('/api/wine/:id?', api.wine.getWine);

    // add a wine
    app.post('/api/wine', api.wine.postWine);

    // update a wine
    app.put('/api/wine/:id', api.wine.putWine);

    // delete a wine
    app.delete('/api/wine/:id', api.wine.deleteWine);


    /* ---- Country ---- */

    // get country
    app.get('/api/country/:id?', api.country.getCountry);

    // add a country
    app.post('/api/country', api.country.postCountry);

    // update a country
    app.put('/api/country/:id', api.country.putCountry);

    // delete a country
    app.delete('/api/country/:id', api.country.deleteCountry);


    /* ---- GRAPE ---- */

    // get grape
    app.get('/api/grape/:id?', api.grape.getGrape);

    // add a grape
    app.post('/api/grape', api.grape.postGrape);

    // update a grape
    app.put('/api/grape/:id', api.grape.putGrape);

    // delete a grape
    app.delete('/api/grape/:id', api.grape.deleteGrape);
};
