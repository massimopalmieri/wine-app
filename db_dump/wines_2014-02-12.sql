# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.71)
# Database: wines
# Generation Time: 2014-02-12 08:41:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT '',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES
	(1,'italy','2014-02-11 19:21:39','2014-02-11 19:21:39',NULL),
	(2,'france','2014-02-11 19:21:43','2014-02-11 19:21:43',NULL);

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table grapes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grapes`;

CREATE TABLE `grapes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `grapes` WRITE;
/*!40000 ALTER TABLE `grapes` DISABLE KEYS */;

INSERT INTO `grapes` (`id`, `name`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES
	(1,'sangiovese','2014-02-11 19:19:12','2014-02-11 19:19:12',NULL),
	(2,'aglianico','2014-02-11 19:20:01','2014-02-11 19:20:01',NULL),
	(3,'cabernet sauvignon','2014-02-11 19:21:28','2014-02-11 19:21:28',NULL);

/*!40000 ALTER TABLE `grapes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wines`;

CREATE TABLE `wines` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `grapeId` int(11) DEFAULT NULL,
  `countryId` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wines` WRITE;
/*!40000 ALTER TABLE `wines` DISABLE KEYS */;

INSERT INTO `wines` (`id`, `name`, `grapeId`, `countryId`, `createdAt`, `updatedAt`, `deletedAt`)
VALUES
	(1,'Principe Strozzi Morellino di Scansano 2011',1,1,'2014-02-11 19:23:01','2014-02-11 19:23:01',NULL),
	(2,'Fattoria Lornano Le Macchie Chianti Classico 2009',1,1,'2014-02-11 19:23:03','2014-02-11 19:23:03',NULL),
	(3,'Canonica a Cerreto \'Canto del Diavolo\' Chianti Classico Riserva 2008',1,1,'2014-02-11 19:23:15','2014-02-11 19:23:15',NULL),
	(4,'Nespolino Sangiovese 2011',1,1,'2014-02-11 19:36:29','2014-02-11 19:36:29',NULL),
	(5,'Principe Strozzi Chianti 2011',1,1,'2014-02-11 19:37:32','2014-02-11 19:37:32',NULL);

/*!40000 ALTER TABLE `wines` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
